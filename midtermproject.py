from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
#Above lines have all necessary imports to interface with ROS and some neccesary math functions

#Create ROS node and Moveit Commander Link
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
#Create robot object as well as a scene variable for the robot's environment
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
#Create joint group for ur5 robot
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
#Command to display trajectory in RVis
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)
#Move Robot to more ideal Starting Configuration
tau = 2.0 * pi #declare angle of 360 degrees
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = 0
joint_goal[3] = -tau / 4
joint_goal[4] = 0
joint_goal[5] = tau / 6  

#This function intakes a 1x3 matrix that has the x,y, and z coordinates of the desired position for the end effector. The function moves the robot to the 
#position specified by this matrix
def moverobot(points):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = points[0]
    pose_goal.position.y = points[1]
    pose_goal.position.z = points[2]
    move_group.set_pose_target(pose_goal)
    # `go()` returns a boolean indicating whether the planning and execution was successful.
    success = move_group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    move_group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets().
    move_group.clear_pose_targets()

allpoints = [] #initialize an empty list to contain all of the coordinates to move the robot to

#Add points for the letter A
allpoints.append([.681,.311,.575])
allpoints.append([.759, .259, .5])
allpoints.append([.681,.311,.575])
allpoints.append([.671,.455,.452])
allpoints.append([.676,.433,.505])
allpoints.append([.683,.148,.516])


#Add points for the letter N
allpoints.append([.681,.311,.575])
allpoints.append([.723, .233,.501])
allpoints.append([.645,.379,.606])
allpoints.append([.648,.504,.506])
allpoints.append([.633,.49,.541])


#Add points for the letter T
allpoints.append([.681,.311,.575])
allpoints.append([.569,.522,.542])
allpoints.append([.769,.0853,.566])
allpoints.append([.681,.311,.575])
allpoints.append([.744,.294,.356])


#For Each loop the allpoints list with robot moving function, to move the robot to every desired point
for x in allpoints:
    moverobot(x)




















